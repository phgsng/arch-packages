# packages
LUAMD5     = lua-md5-git
LUASEC     = luasec-prosody-git
LUAWORK    = lua5.3
MUTT_NNTP  = mutt-nntp
SLNUNICODE = slnunicode-git
SWIG       = swig-git

# collections
PKGS   = $(SLNUNICODE) $(LUAMD5) $(SWIG) $(MUTT_NNTP)
PKGS  += $(LUASEC) $(LUAWORK)
VERFY  = $(PKGS:%=verify-%)
SOURCE = $(PKGS:%=source-%)

MAKEPKG = $(shell which makepkg)

# collective targets

all: $(PKGS)

$(PKGS):
	@echo "==> building package $@ <=="
	cd $@ && $(MAKEPKG) --force
	@echo

verify: $(VERFY)

$(VERFY):
	@echo "==> verifying package $(@:verify-%=%) <=="
	cd $(@:verify-%=%) && $(MAKEPKG) --verify --force
	@echo

source: $(SOURCE)

$(SOURCE):
	@echo "==> building source package $(@:source-%=%) <=="
	cd $(@:source-%=%) && "$(MAKEPKG)" --source --force
	@echo

# individual targets
lua5.3:     $(LUAWORK)
luamd5:     $(LUAMD5)
luasec:     $(LUASEC)
mutt-nntp:  $(MUTT_NNTP)
slnunicode: $(SLNUNICODE)
swig:       $(SWIG)

info:
	@echo settings:
	@echo    "MAKEPKG  = $(MAKEPKG)"
	@echo
	@echo packages:
	@echo    "PKGS     = $(PKGS)"
	@echo
	@echo target categories:
	@echo    "all verify source"
	@echo
	@echo package targets:
	@echo    "slnunicode" "luamd5" "swig"
	@echo    "mutt-nntp" "luasec" "lua5.3"

.PHONY: info all $(PKGS) clean $(VERFY) source $(SOURCE)
